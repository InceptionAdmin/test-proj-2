﻿using System;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Build.Reporting;
#endif
using UnityEngine;

public static class CustomBuild {
#if UNITY_EDITOR

	private static bool _buildStarted;
	
	//[UnityEditor.Callbacks.DidReloadScripts]
	private static void OnScriptsReloaded()
	{
		// TODO: remove?
		try
		{
			if (_buildStarted)
			{
				Debug.Log("Scripts compile finished");
				ExecuteBuild();
			}
		}
		finally
		{
			Cleanup();
		}
	}

	private static void Cleanup()
	{
		_buildStarted = false;
	}

	// Use this for initialization
	[MenuItem("Build/BuildForAndroid")]
	public static void BuildForAndroid()
	{
		Debug.Log("$ --- Starting custom build --- $");
		_buildStarted = true;
		SwitchPlatformToAndroid();
		// System.Threading.Thread.Sleep(15000);
		ExecuteBuild();
		//AssetDatabase.Refresh();	// force compilation
	}
	
	[MenuItem("Build/ExecuteBuild")]
	private static void ExecuteBuild()
	{
		try
		{
			var options = SetBuildOptions();

			var report = BuildPipeline.BuildPlayer(options);
			var errors = report.summary.totalErrors;
			var warnings = report.summary.totalWarnings;
			var totalTime = report.summary.totalTime;
			var result = report.summary.result;
			var size = report.summary.totalSize;

			foreach (var step in report.steps)
			{
				Debug.LogFormat("Buildstep {0} (time: {1})",
					step.name, step.duration);
				foreach (var msg in step.messages)
				{
					Debug.LogFormat("\t[{0}] {1}",
						msg.type, msg.content);
				}
			}
			
			Debug.LogFormat("Build result: {0}\nErrors: {1}\nWarnings: {2}\nSize: {3}\nTime: {4}", result, errors, warnings,
				size, totalTime);
			
			if (result != BuildResult.Succeeded)
			{
				EditorApplication.Exit(1);
			}
		}
		catch (Exception e)
		{
			Debug.LogErrorFormat("Could not build application! Exc: {0}", e);
			Cleanup();
			EditorApplication.Exit(1);
		}
	}

	private static BuildPlayerOptions SetBuildOptions()
	{
		var options = new BuildPlayerOptions();
		
		// set build output path
		var location = SetBuildOutputLocation(options);
		
		options.targetGroup = EditorUserBuildSettings.selectedBuildTargetGroup;
		options.target = EditorUserBuildSettings.activeBuildTarget;
		options.locationPathName = location; // string.IsNullOrEmpty(newPath) ? location : newPath;
		options.scenes = new string[EditorBuildSettings.scenes.Length];
		for (var i = 0; i < EditorBuildSettings.scenes.Length; i++)
		{
			options.scenes[i] = EditorBuildSettings.scenes[i].path;
		}

		options.options = BuildOptions.None;

		// '|=' to add '&= ~' to remove
		if (EditorUserBuildSettings.development)
		{
			options.options |= BuildOptions.Development;
		}

		if (EditorUserBuildSettings.allowDebugging)
		{
			options.options |= BuildOptions.AllowDebugging;
		}

		if (EditorUserBuildSettings.connectProfiler)
		{
			options.options |= BuildOptions.ConnectWithProfiler;
		}

		if (EditorUserBuildSettings.buildScriptsOnly)
		{
			options.options |= BuildOptions.BuildScriptsOnly;
		}

		if (Application.isEditor)
		{
			options.options |= BuildOptions.ShowBuiltPlayer;
		}

		return options;
	}

	private static string SetBuildOutputLocation(BuildPlayerOptions options)
	{
		string location = null;
		if (IsEditorInCliMode())
		{
			location = GetCliBuildOutputPath();
		}

		if (string.IsNullOrEmpty(location))
		{
			location = EditorUserBuildSettings.GetBuildLocation(options.target);

			if (!IsEditorInCliMode() && string.IsNullOrEmpty(location))
			{
				// if Editor is in GUI mode, and no location set
				location = EditorUtility.SaveFilePanel("Select Build Location", location, "", "apk");
			}

			if (string.IsNullOrEmpty(location))
			{
				Debug.LogError("Could not set Build output path!");
				return null;
			}
		}

		return location;
	}

	[MenuItem("Build/SwitchPlatformAndroid")]
	public static void SwitchPlatformToAndroid()
	{
		Debug.Log("Switching Platform to android");
		try
		{
			EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);
			Debug.Log("Finished SwitchPlatform to android");
		}
		catch (Exception e)
		{
			Debug.LogErrorFormat("Could not switch platform! Exc: {0}", e);
			Cleanup();
			EditorApplication.Exit(1);
		}
	}


	private static string GetCliBuildOutputPath()
	{
		var args = Environment.GetCommandLineArgs();
		const string pathFlagName = "-buildOutput";
		for (var i = 0; i < args.Length; i++)
		{
			if (args[i] == pathFlagName)
			{
				if (args.Length == i + 1)
				{
					Debug.LogErrorFormat("Could not find build output path!");
					return null;
				}
				
				var path = args[i + 1];
				Debug.LogFormat("Found build output path: {0}", path);
				return path;
			}
		}

		return null;
	}
	private static bool IsEditorInCliMode()
	{
		var args = Environment.GetCommandLineArgs();
		const string cliFlag = "-nographics";
		for (var i = 0; i < args.Length; i++)
		{
			if (args[i] == cliFlag)
			{
				return true;
			}
		}

		return false;
	}
#endif
}
